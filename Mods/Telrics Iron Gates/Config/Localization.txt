Key,english,japanese

------------------Blocks---------------------------
IronGateGroupDesc,"[e7a729]Iron Gates come in different sizes. They can be opened and closed as well as locked, like any other secure door. Will place towards you, unless you rotate.[-]","[e7a729]Japanese[-]"

IronGate5x3Block,"[e7a729]Iron Gate: 5x3[-]","[e7a729]Japanese[-]"
IronGate4x3Block,"[e7a729]Iron Gate: 4x3[-]","[e7a729]Japanese[-]"
IronGate4x2Block,"[e7a729]Iron Gate: 4x2[-]","[e7a729]Japanese[-]"
IronGate3x2Block,"[e7a729]Iron Gate: 3x2[-]","[e7a729]Japanese[-]"
IronGate3x1Block,"[e7a729]Iron Gate: 3x1[-]","[e7a729]Japanese[-]"
IronGate2x1Block,"[e7a729]Iron Gate: 2x1[-]","[e7a729]Japanese[-]"
